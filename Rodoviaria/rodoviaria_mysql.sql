CREATE DATABASE rodoviaria;
USE rodoviaria;

CREATE TABLE passageiro (
  idPassageiro int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nome varchar(30),
  genero varchar(30),
  RG varchar(20),
  CPF varchar(20),
  endereco varchar(50),
  email varchar(50),
  telefone varchar(30) 
);
