package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JFAlterarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtRG;
	private JTextField txtCPF;
	private JTextField txtEndereco;
	private JTextField txtEmail;
	private JTextField txtTelefone;
	private static int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFAlterarPassageiro frame = new JFAlterarPassageiro(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param id2 
	 */
	public JFAlterarPassageiro(int id) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 726, 593);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		PassageiroDAO pdao = new PassageiroDAO();
		Passageiro p = pdao.read(id);
		
		JLabel lblIdp = new JLabel("ID Passageiro:");
		lblIdp.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 18));
		lblIdp.setBounds(462, 14, 138, 20);
		contentPane.add(lblIdp);
		
		JLabel lblID = new JLabel("New label");
		lblID.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 18));
		lblID.setBounds(614, 18, 98, 13);
		contentPane.add(lblID);
		
		JLabel lblCadastrarPassageiro = new JLabel("Alterar passageiro");
		lblCadastrarPassageiro.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 18));
		lblCadastrarPassageiro.setBounds(10, 10, 213, 28);
		contentPane.add(lblCadastrarPassageiro);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setFont(new Font("Arial", Font.BOLD, 14));
		lblNome.setBounds(20, 48, 68, 15);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 64, 689, 28);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblGenero = new JLabel("Gen\u00EAro");
		lblGenero.setFont(new Font("Arial", Font.BOLD, 14));
		lblGenero.setBounds(20, 102, 86, 13);
		contentPane.add(lblGenero);
		
		JLabel lblRG = new JLabel("RG");
		lblRG.setFont(new Font("Arial", Font.BOLD, 14));
		lblRG.setBounds(20, 163, 45, 13);
		contentPane.add(lblRG);
		
		txtRG = new JTextField();
		txtRG.setBounds(10, 180, 689, 28);
		contentPane.add(txtRG);
		txtRG.setColumns(10);
		
		JLabel lblCPF = new JLabel("CPF");
		lblCPF.setFont(new Font("Arial", Font.BOLD, 14));
		lblCPF.setBounds(20, 218, 45, 13);
		contentPane.add(lblCPF);
		
		txtCPF = new JTextField();
		txtCPF.setBounds(10, 241, 689, 28);
		contentPane.add(txtCPF);
		txtCPF.setColumns(10);
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o");
		lblEndereco.setFont(new Font("Arial", Font.BOLD, 14));
		lblEndereco.setBounds(20, 279, 86, 13);
		contentPane.add(lblEndereco);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(10, 302, 689, 28);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JLabel lblEmail = new JLabel("E-mail");
		lblEmail.setFont(new Font("Arial", Font.BOLD, 14));
		lblEmail.setBounds(20, 340, 68, 13);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(10, 363, 689, 28);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setFont(new Font("Arial", Font.BOLD, 14));
		lblTelefone.setBounds(20, 401, 86, 13);
		contentPane.add(lblTelefone);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(10, 424, 689, 33);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JRadioButton rdbFeminino = new JRadioButton("Feminino");
		rdbFeminino.setFont(new Font("Arial", Font.BOLD, 14));
		rdbFeminino.setBounds(10, 125, 103, 21);
		contentPane.add(rdbFeminino);
		
		JRadioButton rdbMasculino = new JRadioButton("Masculino");
		rdbMasculino.setFont(new Font("Arial", Font.BOLD, 14));
		rdbMasculino.setBounds(120, 126, 103, 21);
		contentPane.add(rdbMasculino);
		
		ButtonGroup genero = new ButtonGroup();
		genero.add(rdbFeminino);
		genero.add(rdbMasculino);
		
		lblID.setText(String.valueOf(p.getIdPassageiro()));
		txtNome.setText(p.getNome());
		if(rdbFeminino.isSelected()) {
			p.setGenero("Feminino");
		}else if(rdbMasculino.isSelected()) {
			p.setGenero("Masculino");
		}
		txtRG.setText(p.getRG());
		txtCPF.setText(p.getCPF());
		txtEndereco.setText(p.getEndereco());
		txtEmail.setText(p.getEmail());
		txtTelefone.setText(p.getTelefone());
		
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Passageiro p = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				p.setIdPassageiro(Integer.parseInt(lblID.getText()));
				
				p.setNome(txtNome.getText());
				if(rdbFeminino.isSelected()) {
					p.setGenero("Feminino");
				}else if(rdbMasculino.isSelected()) {
					p.setGenero("Masculino");
				}
				p.setRG(txtRG.getText());
				p.setCPF(txtCPF.getText());
				p.setEndereco(txtEndereco.getText());
				p.setEmail(txtEmail.getText());
				p.setTelefone(txtTelefone.getText());
				
				dao.update(p);
				dispose();
			}
		});
		
		btnAlterar.setFont(new Font("Arial", Font.BOLD, 14));
		btnAlterar.setBounds(10, 494, 122, 33);
		contentPane.add(btnAlterar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNome.setText(null);
				genero.clearSelection();
				txtRG.setText(null);
				txtCPF.setText(null);
				txtEndereco.setText(null);
				txtEmail.setText(null);
				txtTelefone.setText(null);
			}
		});
		btnLimpar.setFont(new Font("Arial", Font.BOLD, 14));
		btnLimpar.setBounds(159, 494, 112, 33);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setFont(new Font("Arial", Font.BOLD, 14));
		btnCancelar.setBounds(495, 494, 130, 33);
		contentPane.add(btnCancelar);
	}

}