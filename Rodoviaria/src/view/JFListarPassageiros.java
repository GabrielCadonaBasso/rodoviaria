package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class JFListarPassageiros extends JFrame {

	private JPanel contentPane;
	private JTable JTPassageiros;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFListarPassageiros frame = new JFListarPassageiros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFListarPassageiros() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				readJTable();
			}
		});
		setTitle("Listar Passageiros");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1001, 561);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblListarPassageiros = new JLabel("Listar Passageiros");
		lblListarPassageiros.setFont(new Font("Montserrat SemiBold", Font.PLAIN, 25));
		lblListarPassageiros.setBounds(339, 24, 245, 37);
		contentPane.add(lblListarPassageiros);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 66, 967, 332);
		contentPane.add(scrollPane);
		
		JTPassageiros = new JTable();
		JTPassageiros.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null, null, null, null, null},
				{null, null, null, null, null, null, null, null},
			},
			new String[] {
				"idPassageiro", "Nome", "Gen\u00EAro", "RG", "CPF", "Endere\u00E7o", "Email", "Telefone"
			}
		));
		scrollPane.setViewportView(JTPassageiros);
		
		JButton btnCadastrarPassageiro = new JButton("Cadastrar Passageiro");
		btnCadastrarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFCadastrarPassageiro cp = new JFCadastrarPassageiro();
				cp.setVisible(true);
				
			}
		});
		btnCadastrarPassageiro.setFont(new Font("Arial", Font.BOLD, 15));
		btnCadastrarPassageiro.setBounds(10, 419, 203, 42);
		contentPane.add(btnCadastrarPassageiro);
		
		JButton btnAlterarPassageiro = new JButton("Alterar Passageiro");
		btnAlterarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//verificar a linha selecionada
				if(JTPassageiros.getSelectedRow() != -1) {
					JFAlterarPassageiro ap = new JFAlterarPassageiro((int)JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(), 0));
					ap.setVisible(true);
					
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um filme!");
				}
				readJTable();
			}
		});
		btnAlterarPassageiro.setFont(new Font("Arial", Font.BOLD, 15));
		btnAlterarPassageiro.setBounds(259, 419, 220, 42);
		contentPane.add(btnAlterarPassageiro);
		
		JButton btnExcluirPassageiro = new JButton("Excluir Passageiro");
		btnExcluirPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
                if(JTPassageiros.getSelectedRow()!= -1) {
                    int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o passageiro selecionado?","Exclusão", JOptionPane.YES_NO_OPTION);
                    if(opcao == 0) {
                        PassageiroDAO dao = new PassageiroDAO();
                        Passageiro p = new Passageiro();
                        p.setIdPassageiro((int) JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(),0));
                        dao.delete(p);
                    }
                }else {
                    JOptionPane.showMessageDialog(null, "Selecione um filme!");
                }
                readJTable();
            }
		});
		btnExcluirPassageiro.setFont(new Font("Arial", Font.BOLD, 15));
		btnExcluirPassageiro.setBounds(774, 419, 203, 42);
		contentPane.add(btnExcluirPassageiro);
		
		readJTable();
	}
	
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JTPassageiros.getModel();
		modelo.setNumRows(0);
		PassageiroDAO pdao = new PassageiroDAO();
		for(Passageiro p : pdao.read()) {
			modelo.addRow(new Object[] {
					p.getIdPassageiro(),
					p.getNome(),
					p.getGenero(),
					p.getRG(),
					p.getCPF(),
					p.getEndereco(),
					p.getEmail(),
					p.getTelefone()
			});
			
		}
	}
}